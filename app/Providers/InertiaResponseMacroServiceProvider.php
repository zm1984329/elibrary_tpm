<?php

namespace App\Providers;
use Illuminate\Http\Response;
use Illuminate\Support\ServiceProvider;

class InertiaResponseMacroServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Response::macro('withHeaders', function (array $headers) {
            $response = Inertia::location(url()->previous());

            foreach ($headers as $name => $value) {
                $response->header($name, $value);
            }

            return $response;
        });
    }
}
