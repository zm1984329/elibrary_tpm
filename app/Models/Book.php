<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Book extends Model
{
    use HasFactory;

    use SoftDeletes;
    protected $table = 'books';
    public $incrementing = true;

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';

    protected $fillable = [
        'id',
        'title',
        'author',
        'category_id',
        'type_id',
        'isbn',
        'volume',
        'lieu',
        'edition',
        'date_edition',
        'page',
        'description',
        'cover_image',
        'path'
    ];
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->firstOrFail();
    }
    public function scopeFilter($query, array $filters)
    {

            $query->when($filters['category'] ?? null, function ($query, $category) {
                $query->where('category_id','=',$category);
             })->when($filters['search'] ?? null, function ($query, $search) {
                 $query->where('title', 'like', '%'.$search.'%')
                 ->orWhere('author','like','%'.$search.'%');
             });

    }
}
