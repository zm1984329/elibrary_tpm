<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Categories/index', [
            'filters' => ['search'=>$request->search],
            'categories' => Category:: orderBy('label')
                ->filter($request->only('search'))
                ->paginate(10)
                ->withQueryString(),
        ]);
    }
    public function create()
    {
        return Inertia::render('Categories/Create');
    }
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'id_type'=> ['required'],
            'label' => ['required', 'max:100'],
        ]);
        Category::create([
            'id_type'=>$request->id_type,
            'label' =>$request->label
        ]);

        return Redirect::route('categories')->with('success', 'Category created.');
    }
    public function edit(Category $category)
    {
        return Inertia::render('Categories/Edit', [
            'category' => [
                'id' => $category->id,
                'id_type' => $category->id_type,
                'label' => $category->label,
                'deleted_at' => $category->deleted_at,
            ],
        ]);
    }
    public function update(Request $request,Category $category)
    {
        $validator=Validator::make($request->all(),[
            'label' => ['required', 'max:100'],
        ]);
        $category->update(
            [
                'id_type'=>$request->id_type,
                'label' => $request->label,
            ]
        );

        return Redirect::back()->with('success', 'Category updated.');
    }
    public function destroy(Category $category)
    {
        $category->delete();

        return Redirect::route('categories')->with('success', 'Category deleted.');
    }

    public function restore(Category $category)
    {
        $category->restore();

        return Redirect::back()->with('success', 'Category restored.');
    }
}
