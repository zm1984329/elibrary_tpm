<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Inertia\Inertia;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    //
    public function index(Request $request)
    {
        return Inertia::render('Books/Index', [
            'filters' => ['search'=>$request->search,'type'=>$request->type],
            'books' => Book:: orderBy('title')
                ->where('type_id',1)
                ->filter($request->only('search','type'))
                ->paginate(10)
                ->withQueryString(),
            'periodiques'=> Book::orderBy('title')
            ->where('type_id',2)
            ->filter($request->only('search','category'))
            ->paginate(10)
            ->withQueryString(),
                /*
            'classique'=> Book:: orderBy('title')
            ->filter($request->only('search'))
            ->paginate(10)
            ->withQueryString(), */
        ]);
    }

    public function create()
    {
        $categories=Category::all();
        return Inertia::render('Books/Create',[
            'categories'=>$categories,]);
    }
    public function list(Request $request)
    {
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="SA.pdf"',
        ];
        $user=Auth::user();
            $user->photo=$user->photo_path ? URL::route('image', ['path' => $user->photo_path, 'w' => 40, 'h' => 40, 'fit' => 'crop']) : null;

        return Inertia::render('Books/list', [
            'filters' => ['search'=>$request->search,'category'=>$request->category],
            'books' => Book::orderBy('title')
                ->where('type_id',1)
                ->filter($request->only('search','category','type'))
                ->paginate(10)
                ->withQueryString(),
            'periodiques'=> Book::orderBy('title')
            ->where('type_id',2)
            ->filter($request->only('search','category'))
            ->paginate(10)
            ->withQueryString(),
            'categories'=> Category::all(),
            'user'=>$user,

        ]);
    }
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $validator=Validator::make($request->all(),[
                'title' => ['required', 'max:100'],
                'author' => ['nullable', 'max:50'],
                'type_id'=>['required'],
                'volume'=> ['nullable', 'max:50'],
                'lieu'=> ['nullable', 'max:50'],
                'edition'=> ['nullable', 'max:50'],
                'date_edition'=> ['nullable', 'max:50'],
                'page'=> ['nullable', 'max:50'],
                'category_id' => ['nullable', 'max:50'],
                'isbn' => ['nullable', 'max:150'],
                'description' => ['nullable', 'max:50'],
            ]);

            if ($request->hasFile('pdf') && $request->hasFile('cover_image')) {
                if ($request->file('pdf')->isValid() && $request->file('cover_image')->isValid()) {
                    $file = $request->file('pdf');
                    $image=$request->file('cover_image');
                    $name = $file->getClientOriginalName();
                    $nameImage=$image->getClientOriginalName();
                    $path = $request->pdf->storeAs('pdf', $name , 'public');
                    $cover_image = $request->cover_image->storeAs('image', $nameImage , 'public');
                    Book::create([
                            'title' =>$request->title,
                            'author' => $request->author,
                            'type_id'=>$request->type_id,
                            'category_id' => $request->category_id,
                            'isbn' => $request->isbn,
                            'volume'=>  $request->volume,
                            'lieu'=>  $request->lieu,
                            'edition'=>  $request->edition,
                            'date_edition'=>  $request->date_edition,
                            'page'=>  $request->page,
                            'description' => $request->description,
                            'cover_image' => $cover_image,
                            'path' => $path,
                        ]
                    );

                DB::commit();
                return Redirect::route('books')->with('success', 'Book created.');
                }
            }
                DB::rollback();
                return Redirect::route('books')->with('success', 'upload book failed.');
    }
    catch (\Throwable $th) {
        DB::rollback();
        return Redirect::route('books')->with('success', $th->getMessage());
    }}
    public function edit(Book $book)
    {
        return Inertia::render('Books/Edit', [
            'book' => [
                'id' => $book->id,
                'title' => $book->title,
                'author' => $book->author,
                'type_id'=>$book->type_id,
                'category_id' => $book->category_id,
                'isbn' => $book->isbn,
                'volume'=>  $book->volume,
                'lieu'=>  $book->lieu,
                'edition'=>  $book->edition,
                'date_edition'=>  $book->date_edition,
                'page'=>  $book->page,
                'description' => $book->description,
                'cover_image' => $book->cover_image,
                'path' => $book->path,
                'deleted_at' => $book->deleted_at,
            ],
            'categories'=> Category::all(),
        ]);
    }

    public function update(Request $request, Book $book)
    {
        $validator=Validator::make($request->all(),[
            'title' => ['required', 'max:100'],
            'author' => ['nullable', 'max:50'],
            'category_id' => ['nullable', 'max:50'],
            'isbn' => ['nullable', 'max:150'],
            'volume'=> ['nullable', 'max:50'],
            'lieu'=> ['nullable', 'max:50'],
            'edition'=> ['nullable', 'max:50'],
            'date_edition'=> ['nullable', 'max:50'],
            'page'=> ['nullable', 'max:50'],
            'description' => ['nullable', 'max:50'],
        ]);

            $book->update([
                'title' =>$request->title,
                'author' => $request->author,
                'type_id'=>$request->type_id,
                'category_id' => $request->category_id,
                'isbn' => $request->isbn,
                'volume'=>  $request->volume,
                'lieu'=>  $request->lieu,
                'edition'=>  $request->edition,
                'date_edition'=>  $request->date_edition,
                'page'=>  $request->page,
                'description' => $request->description,
                ]
            );
            if ($request->hasFile('path') && $request->hasFile('cover_image')) {
                Storage::delete($book->path);
                Storage::delete($file->cover_image);
                $file = $request->file('path');
                $image=$request->file('cover_image');
                $name = $file->getClientOriginalName();
                $nameImage=$image->getClientOriginalName();
                $path = $request->path->storeAs('pdf', $name , 'public');
                $cover_image = $request->cover_image->storeAs('image', $nameImage , 'public');
                $book->update([
                    'cover_image'=>$cover_image,
                    'path'=>$path,
                    ]
                );}


        return Redirect::back()->with('success', 'Book updated.');
    }

    public function destroy(Book $book)
    {

        if($book->path && $book->cover_image){
            if (Storage::exists('public/'.$book->path) && Storage::exists('public/'.$book->cover_image)) {
                Storage::delete('public/'.$book->path);
                Storage::delete('public/'.$book->cover_image);
            }
        }
        $book->delete();
        return Redirect::route('books')->with('success', 'Book deleted.');
    }
    public function about(){
        $user=Auth::user();
            $user->photo=$user->photo_path ? URL::route('image', ['path' => $user->photo_path, 'w' => 40, 'h' => 40, 'fit' => 'crop']) : null;

        return Inertia::render('Books/propos',
        ['user'=>$user]);
    }
    public function restore(Book $book)
    {
        $book->restore();

        return Redirect::back()->with('success', 'Book restored.');
    }
}
