<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\UsersController;

use App\Http\Controllers\ImagesController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\OrganizationsController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth

Route::get('login', [AuthenticatedSessionController::class, 'create'])
    ->name('login')
    ->middleware('guest');

Route::post('login', [AuthenticatedSessionController::class, 'store'])
    ->name('login.store')
    ->middleware('guest');

Route::delete('logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');

// Dashboard

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->middleware('owner');

// Users

Route::get('users', [UsersController::class, 'index'])
    ->name('users')
    ->middleware('auth');

Route::get('users/create', [UsersController::class, 'create'])
    ->name('users.create')
    ->middleware('owner');

Route::post('users', [UsersController::class, 'store'])
    ->name('users.store')
    ->middleware('auth');

Route::get('users/{user}/edit', [UsersController::class, 'edit'])
    ->name('users.edit')
    ->middleware('owner');

Route::put('users/{user}', [UsersController::class, 'update'])
    ->name('users.update')
    ->middleware('owner');

Route::delete('users/{user}', [UsersController::class, 'destroy'])
    ->name('users.destroy')
    ->middleware('owner');

Route::put('users/{user}/restore', [UsersController::class, 'restore'])
    ->name('users.restore')
    ->middleware('owner');
//categories
Route::get('categories/create', [CategoryController::class, 'create'])
    ->name('categories.create')
    ->middleware('owner');
Route::post('categories', [CategoryController::class, 'store'])
    ->name('categories.store')
    ->middleware('owner');
Route::get('categories', [CategoryController::class, 'index'])
    ->name('categories')
    ->middleware('auth');

Route::get('categories/{category}/edit', [CategoryController::class, 'edit'])
    ->name('categories.edit')
    ->middleware('owner');

Route::put('categories/{category}', [CategoryController::class, 'update'])
    ->name('categories.update')
    ->middleware('owner');

Route::delete('categories/{category}', [CategoryController::class, 'destroy'])
    ->name('categories.destroy')
    ->middleware('owner');
Route::put('categories/{category}/restore', [CategoryController::class, 'restore'])
    ->name('categories.restore')
    ->middleware('owner');

Route::get('books/create', [BookController::class, 'create'])
    ->name('books.create')
    ->middleware('owner');
Route::post('books', [BookController::class, 'store'])
    ->name('books.store')
    ->middleware('owner');
Route::get('books/{book}/edit', [BookController::class, 'edit'])
    ->name('books.edit')
    ->middleware('owner');
Route::put('books/{book}', [BookController::class, 'update'])
    ->name('books.update')
    ->middleware('owner');
Route::delete('books/{book}', [BookController::class, 'destroy'])
    ->name('books.destroy')
    ->middleware('owner');
//Book
Route::put('books/{book}/restore', [BookController::class, 'restore'])
    ->name('books.restore')
    ->middleware('owner');
Route::get('books', [BookController::class, 'index'])
    ->name('books')
    ->middleware('owner');
Route::get('/', [BookController::class, 'list'])
    ->name('books.list')
    ->middleware('auth');
Route::get('/about', [BookController::class, 'about'])
    ->name('books.about')
    ->middleware('auth');


// Images

Route::get('/img/{path}', [ImagesController::class, 'show'])
    ->where('path', '.*')
    ->name('image');
